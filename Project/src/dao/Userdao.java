package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class Userdao {

	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);

			ResultSet rst = pStmt.executeQuery();

			if (!rst.next()) {
				return null;
			}
			String loginIdData = rst.getString("login_id");
			String nameData = rst.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}

	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE id !=1";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				if(!(rs.getString("login_id").equals("admin"))) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");


				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public List<User> UserListSearch(String loginId,String user_name,String Sbirth_date, String Ebirth_date){
		Connection conn = null;
		List<User> userlist = new ArrayList<User>();

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE id !=1";

			if(!loginId.equals("")) {
				sql += " AND login_id = '"+loginId+"'";
			}
			if(!user_name.equals("")) {
				sql += " AND name LIKE '%" + user_name + "%'";
			}
			if(!Sbirth_date.equals("")) {
				sql += " AND birth_date >= '" + Sbirth_date + "'";
			}
			if(!Ebirth_date.equals("")) {
				sql += " AND birth_date <= '" + Ebirth_date +"' ";
			}
			System.out.println(sql);

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String LoginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");


				User user = new User(id, LoginId, name, birthDate, password, createDate, updateDate);

				userlist.add(user);
			}

			}catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		return userlist;
	}

	public String cypher(String password) {
		String source = password;

		Charset charset = StandardCharsets.UTF_8;
		String algorithm = "MD5";

		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);

		return result;

	}

	public void RegisterNewAccount(String loginId, String protect,
			String user_name, String birth_date) {
		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "INSERT INTO user (login_id,name,birth_date,password,create_date,update_date) "
					+ "VALUES(?,?,?,?,NOW(),NOW())";

			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, loginId);
			stmt.setString(2, user_name);
			stmt.setString(3, birth_date);
			stmt.setString(4, protect);


			stmt.executeUpdate();

			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public User Searchid(String loginId) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sql =  "SELECT * FROM user WHERE login_id = ?";

			PreparedStatement stmt = conn.prepareStatement(sql);

			stmt.setString(1, loginId);

			ResultSet rs = stmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			int sid = rs.getInt("id");
			String LoginId = rs.getString("login_Id");
			String user_name = rs.getString("name");
			Date birth_date = rs.getDate("birth_date");
			String Password = rs.getString("password");
			String create_date = rs.getString("create_date");
			String update_date = rs.getString("update_date");

			return  new User(sid,LoginId,user_name,birth_date,
								 Password,create_date,update_date);


			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
	}



	public User Searchdetail(String id) {
		Connection conn = null;


		try {
			conn = DBManager.getConnection();

			String sql =  "SELECT * FROM user WHERE id = ?";

			PreparedStatement stmt = conn.prepareStatement(sql);

			stmt.setString(1, id);

			ResultSet rs = stmt.executeQuery();

			if (!rs.next()) {
				return null;
			}


			int sid = rs.getInt("id");
			String LoginId = rs.getString("login_Id");
			String user_name = rs.getString("name");
			Date birth_date = rs.getDate("birth_date");
			String Password = rs.getString("password");
			String create_date = rs.getString("create_date");
			String update_date = rs.getString("update_date");

			return  new User(sid,LoginId,user_name,birth_date,
								 Password,create_date,update_date);


			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
	}

	public void Userupdate(String id,String protect,String user_name,String birthDate) {
		Connection conn = null;


		try {

			conn = DBManager.getConnection();

			String sql = "UPDATE user SET name = ? , birth_date = ?, "
					+ "password = ? WHERE id = ?;";

			PreparedStatement stmt = conn.prepareStatement(sql);

			stmt.setString(4, id);
			stmt.setString(3, protect);
			stmt.setString(1, user_name);
			stmt.setString(2, birthDate);

			stmt.executeUpdate();

			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	public void UserupdateP(String id,String user_name,String birthDate) {
		Connection conn = null;


		try {

			conn = DBManager.getConnection();

			String sql = "UPDATE user SET name = ? , birth_date = ? "
					+ " WHERE id = ?;";

			PreparedStatement stmt = conn.prepareStatement(sql);

			stmt.setString(3, id);
			stmt.setString(1, user_name);
			stmt.setString(2, birthDate);

			stmt.executeUpdate();

			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void UserDelete(String id) {
		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "DELETE FROM user WHERE id = ?;";

			PreparedStatement stmt = conn.prepareStatement(sql);

			stmt.setString(1,id);

			stmt.executeUpdate();
			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}





