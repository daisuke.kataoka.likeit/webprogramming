package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.Userdao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();

    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String id = request.getParameter("id");

		Userdao userDao = new Userdao();
		User user = userDao.Searchdetail(id);

		request.setAttribute("user",user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Information_update.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");
		String password = request.getParameter("password");
		String cPassword = request.getParameter("check_password");
		String user_name = request.getParameter("user_name");
		String birthDate = request.getParameter("birth_date");



		if (!password.equals(cPassword)) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			request.setAttribute("user.id", id);
			request.setAttribute("user.name", user_name);
			request.setAttribute("user.birthDate", birthDate);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Information_update.jsp");
			dispatcher.forward(request, response);
			return;
		} else if (user_name == null || user_name.equals("") ||
				birthDate == null || birthDate.equals("")
				) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			request.setAttribute("id", id);
			request.setAttribute("user-name", user_name);
			request.setAttribute("birth_date", birthDate);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Information_update.jsp");
			dispatcher.forward(request, response);
			return;
		} else if(password.equals("") && cPassword.equals("")){
			Userdao userdao = new Userdao();
			userdao.UserupdateP(id, user_name, birthDate);
			response.sendRedirect("UserListServlet");
		}else {
		Userdao userdao = new Userdao();
		String protect = userdao.cypher(password);
		userdao.Userupdate(id,protect,user_name,birthDate);
		response.sendRedirect("UserListServlet");
	}
	}
}
