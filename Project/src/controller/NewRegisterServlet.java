package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.Userdao;

/**
 * Servlet implementation class NewRegisterServlet
 */
@WebServlet("/NewRegisterServlet")
public class NewRegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public NewRegisterServlet() {
		super();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/New＿Legister.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("login-id");
		String password = request.getParameter("password");
		String check_password = request.getParameter("check_password");
		String user_name = request.getParameter("user-name");
		String birth_date = request.getParameter("birth_date");

		Userdao userdao = new Userdao();

		if(!(userdao.Searchid(loginId) == null)) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			request.setAttribute("loginId", loginId);
			request.setAttribute("user-name", user_name);
			request.setAttribute("birth_date", birth_date);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/New＿Legister.jsp");
			dispatcher.forward(request, response);
			return;
		}
		if (!password.equals(check_password)) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			request.setAttribute("login-id", loginId);
			request.setAttribute("user-name", user_name);
			request.setAttribute("birth_date", birth_date);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/New＿Legister.jsp");
			dispatcher.forward(request, response);
			return;
		} else if (loginId == null || loginId.equals("") || password == null || password.equals("") ||
				check_password == null || check_password.equals("") ||
				user_name == null || user_name.equals("") ||
				birth_date == null || birth_date.equals("")) {
			request.setAttribute("errMsg", "未入力があります");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/New＿Legister.jsp");
			dispatcher.forward(request, response);
			return;
		} else {
			Userdao userDao = new Userdao();
			String protect = userDao.cypher(password);
			userDao.RegisterNewAccount(loginId,protect, user_name, birth_date);

			response.sendRedirect("UserListServlet");
		}
	}
}
