<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ更新</title>
<link rel="stylesheet"
 href ="style１.css">
</head>
<body>



	<p align="right">
		<font color="white">
			${userInfo.name} さん
		</font>

			<a href="LogoutServlet" class="stretched-link">
				<font color="red">
				ログアウト
				</font>
			</a>
	</p>

	<h1>
	ユーザ情報更新
	</h1>

	<c:if test="${errMsg != null}" >
	    <div class="alert-danger" role="alert">
	    	<font color="red">
		 	 ${errMsg}
		 	 </font>
		</div>
	</c:if>

	<div class='row'>
		<label for="Inputform">ログインID</label>
			<span></span>
		${user.loginId}
	</div>


	<form method="post" action="UserUpdateServlet" class="form-horizontal">

	<input type="hidden" name="id" value="${user.id}">

	<br>

	<div class='row'>
		<label for="Inputform">パスワード</label>
			<span></span>
		<input type="text" class="form-control" name="password">
	</div>

	<br>

	<div class='row'>
		<label for="Inputform">パスワード(確認)</label>
			<span></span>
		<input type="text" class="form-control" name="check_password">
	</div>

	<br>

	<div class='row'>
		<label for="Inputform">ユーザ名</label>
			<span></span>
		<input type="text" class="form-control" name = user_name value="${user.name}">
	</div>

	<br>

	<div class='row'>
		<label for="Inputform">生年月日</label>
			<span></span>
		<input type="text" class="form-control" name = birth_date value="${user.birthDate}">
	</div>

	<br>
	<br>

	<div class="button">
		<input type="submit" value="更新">
	</div>

	</form>

	<a href="UserListServlet" class="stretched-link-back">
		戻る
	</a>

</body>
</html>