<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
<link rel="stylesheet"
href="style１.css">
</head>
<body>
	<h1>ログイン画面</h1>

	<div class="container">

	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

	<span></span>

	 <form class="form-signin" action="LoginServlet" method="post">

	<div class="row">
		<span>
			<label for="Inputform">ログインID</label>
		</span> <input type="text" class="form-control" name="loginId" >
	</div>

	<br>

	<div class="row">
		<span>
		 <label for="Inputform">パスワード</label>
    	</span> <input type="text" class="form-control" name="password">

	</div>

	<br>

	<div class="button">
    	    <button type="submit">ログイン</button>
	</div>
    </form>

</div>
</body>
</html>
