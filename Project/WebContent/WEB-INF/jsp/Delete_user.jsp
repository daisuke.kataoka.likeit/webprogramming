<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ削除確認</title>
<link rel="stylesheet"
 href ="style１.css">
 <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">

</head>
<body>
	<p align="right">
		<font color="white">
			${userInfo.name} さん
		</font>




			<a href="LogoutServlet" class="stretched-link">
				<font color="red">
				ログアウト
				</font>
			</a>
	</p>

	<h1>
	ユーザ削除確認
	</h1>

	<div class="context">
	　ログインID:${user.loginId}
	<br>を本当に削除してよろしいでしょうか。
	</div>

	<br>
	<br>
	<br>


<div class="row">

	<span></span>

	<span></span>

		<input type="button" value="キャンセル" onclick="history.back()">

	<span></span>

	<form method="post" action="UserDeleteServlet" class="form-horizontal">
		<input type="submit" value="ok"　name="id">
		<input type="hidden" name="id" value="${user.id}">
	</form>

</div>

</body>
</html>