<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ情報詳細参照</title>
<link rel="stylesheet"
 href ="style１.css">
</head>
<body>
	<p align="right">
		<font color="white">
			${userInfo.name} さん
		</font>

			<a href="LogoutServlet" class="stretched-link">
				<font color="red">
				ログアウト
				</font>
			</a>
	</p>

	<h1>
		ユーザ情報詳細参照
	</h1>

	<div class='row'>
		<label for="Inputform">ログインID</label>
			<span></span>
		${user.loginId}
	</div>

	<br>

	<div class='row'>
		<label for="Inputform">ユーザ名</label>
			<span></span>
	${user.name}
	</div>

	<br>

	<div class='row'>
		<label for="Inputform">生年月日</label>
			<span></span>
		${user.birthDate}
	</div>

	<br>

	<div class='row'>
		<label for="Inputform">登録日時</label>
			<span></span>
		${user.createDate}
	</div>

	<br>

	<div class='row'>
		<label for="Inputform">更新日時</label>
			<span></span>
		${user.updateDate}
	</div>

	<br>

	<a href="UserListServlet" class="stretched-link-back">
		戻る
	</a>
</body>
</html>