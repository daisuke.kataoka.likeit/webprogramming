<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー一覧</title>
<link rel="stylesheet"
href="style１.css">
<link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">

</head>
<body>
	<p align="right">
		<font color="white">
			${userInfo.name} さん
		</font>

			<a href="LogoutServlet" class="stretched-link">
				<font color="red">
				ログアウト
				</font>
			</a>

	</p>

	<h1>
		ユーザ一覧
	</h1>

	<a href="NewRegisterServlet" class="stretched-link">
		新規登録
	</a>

	<br>
	<br>

	<form method="post" action="UserListServlet" class="form-horizontal" >
		<div class='row'>
		<label for="Inputform">ログインID</label>
			<span></span>
		<input type="text" class="form-control-l" name="login-id">
		</div>


	<br>

	<div class='row'>
		<label for="Inputform">ユーザ名</label>
			<span></span>
		<input type="text" width="250px" class="form-control-l" name="user-name">
	</div>

	<div class='row'>
		<label for="Inputform">生年月日</label>
			<span></span>
		<input  type="text" class="form-control-date" name="date-start" id="date-start" placeholder="年 / 月/ 日">

			  ~

		<input  type="text" class="form-control-date" name="date-end" id="date-end" placeholder="年 / 月/ 日">
	</div>

	<br>

	<div class="row">
    		<a>    <input type="submit" value="　　検索　　">  </a>
	</div>

	</form>


	<br>
	<hr>

	  <div class="table-responsive">
             <table class="table table-striped">
               <thead>
                 <tr>
                   <th>ログインID</th>
                   <th>ユーザ名</th>
                   <th>生年月日</th>
                   <th></th>
                 </tr>
               </thead>
               <tbody>
                 <c:forEach var="user" items="${userList}">

                   <tr>
                     <td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>
                     <td>

                       <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
					 <c:if test="${userInfo.loginId == 'admin' || userInfo.loginId == user.loginId}" >
                       	<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
					</c:if>
                       <c:if test="${userInfo.loginId == 'admin'}" >
                       	<a class="btn btn-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>
                       </c:if>
                     </td>
                   </tr>
                 </c:forEach>
               </tbody>
             </table>
        </div>



</body>
</html>