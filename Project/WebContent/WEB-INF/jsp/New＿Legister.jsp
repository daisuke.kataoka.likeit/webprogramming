<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ新規登録</title>
<link rel="stylesheet"
 href ="style１.css">

</head>
<body>
	<p align="right">
		<font color="white">
			${userInfo.name} さん
		</font>

			<a href="LogoutServlet" class="stretched-link">
				<font color="red">
				ログアウト
				</font>
			</a>
	</p>

	<h1>
		ユーザ新規登録
	</h1>

	<c:if test="${errMsg != null}" >
	    <div class="alert-danger" role="alert">
	    	<font color="red">
		 	 ${errMsg}
		 	 </font>
		</div>
	</c:if>

	<br>

 	<form method="post" action="NewRegisterServlet" class="form-horizontal">
		<div class='row'>
			<label for="Inputform">ログインID</label>
				<span></span>
			<input type="text" class="form-control" name = "login-id" value="${loginId}">

		</div>

		<div class='row'>
			<label for="Inputform">パスワード</label>
				<span></span>
			<input type="text" class="form-control" name= "password">
		</div>

		<div class='row'>
			<label for="Inputform">パスワード（確認）</label>
				<span></span>
			<input type="text" class="form-control" name= "check_password">
		</div>

		<div class='row'>
			<label for="Inputform">ユーザ名</label>
				<span></span>
			<input type="text" class="form-control" name= "user-name">
		</div>

		<div class='row'>
			<label for="Inputform">生年月日</label>
				<span></span>
			<input type="text" class="form-control" name="birth_date" value="${birth_date}">
		</div>

	<br>

		<div class="text-right">
                  <button type="submit" class="btn btn-primary form-submit">登録</button>
        </div>

	</form>

	<br>

	<a href="UserListServlet" class="stretched-link-back">
		戻る
	</a>

</body>
</html>